package game;

import java.util.concurrent.atomic.AtomicInteger;

public class Counter implements Runnable{
    private AtomicInteger score = new AtomicInteger(100);
    private AtomicInteger totalTime = new AtomicInteger(0);
    private AtomicInteger answerTime = new AtomicInteger(0);
    private Thread thread;

    public synchronized void addScoreOnTimeLimit() {
        score.addAndGet((int) Math.round(0.1 * score.get()));
    }

    public synchronized void addScoreOutOfTimeLimit() {
        score.addAndGet((int) Math.round(0.05 * score.get()));
    }


    public void start(){
        if (thread == null){
            thread = new Thread(this,"counter");
            thread.start();
        }
    }

    @Override
    public void run() {
        try {
            while(true) {
                thread.sleep(1000);
                decrementScore();
                incrementTime();
            }

        }
        catch (InterruptedException e){

        }
    }

    public synchronized void stop(){
        thread.interrupt();
    }

    private synchronized void decrementScore(){
        score.decrementAndGet();
    }

    public synchronized void resetAnswerTime(){
        answerTime.set(0);
    }

    public synchronized void incrementTime(){
        answerTime.incrementAndGet();
        totalTime.incrementAndGet();
    }

    public synchronized int getAnswerTime(){
        return answerTime.decrementAndGet();
    }

    public synchronized int getTotalTime(){
        return totalTime.get() - 10;
    }

    public synchronized int getScore(){
        return score.get();
    }

}
