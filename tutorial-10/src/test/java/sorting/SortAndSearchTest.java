package sorting;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotEquals;

public class SortAndSearchTest {
    private int[] arr;
    private int[] sortedArr;

    @Before
    public void setUp() throws IOException{
        File sortingProblemFile = new File("plainTextDirectory/input/sortingProblem.txt");
        FileReader fileReader = new FileReader(sortingProblemFile);
        arr = new int[50000];
        sortedArr = new int[50000];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;
        int indexOfSequence = 0;
        while ((currentLine = bufferedReader.readLine()) != null) {
            arr[indexOfSequence] = Integer.parseInt(currentLine);
            indexOfSequence++;
        }
        for (int i = 0; i < 50000; i++){
            sortedArr[i] = i+1;
        }
    }

    @Test
    public void sortTest(){
        int[] arraySorted = Sorter.mergeSort(arr);
        assertArrayEquals(sortedArr,arraySorted);
        arr = arraySorted;
    }

    @Test
    public void searchTest(){
        int searchResult = Finder.binarySearch(sortedArr,35712);
        assertNotEquals(-1,searchResult); //search is success
    }
}
