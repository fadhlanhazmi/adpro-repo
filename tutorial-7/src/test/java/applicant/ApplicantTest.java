package applicant;

import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.function.Predicate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class ApplicantTest {
    private Applicant applicant;
    private Predicate<Applicant> qualifiedCheck;
    private Predicate<Applicant> creditCheck;
    private Predicate<Applicant> employmentCheck;
    private Predicate<Applicant> crimeCheck;

    @Before
    public void setUp() {
        applicant = new Applicant();
        qualifiedCheck = Applicant::isCredible;
        creditCheck = theApplicant -> theApplicant.getCreditScore() > 600;
        employmentCheck = theApplicant -> theApplicant.getEmploymentYears() > 0;
        crimeCheck = theApplicant -> !theApplicant.hasCriminalRecord();

    }


    @Test
    public void testCredit() {
        assertTrue(Applicant.evaluate(applicant, qualifiedCheck.and(creditCheck)));
    }

    @Test
    public void testEmploymentAndCredit() {
        assertTrue(Applicant.evaluate(applicant, qualifiedCheck.and(employmentCheck).and(creditCheck)));
    }

    @Test
    public void testEmploymentAndCrime() {
        assertFalse(Applicant.evaluate(applicant, qualifiedCheck.and(employmentCheck).and(crimeCheck)));
    }

    @Test
    public void testEmploymentCreditAndCrime() {
        assertFalse(Applicant.evaluate(applicant, qualifiedCheck.and(employmentCheck).and(creditCheck).and(crimeCheck)));
    }

    @Test
    public void testMain() {
        System.out.println("main");
        final InputStream original = System.in;
        Applicant.main(null);
        System.setIn(original);
    }

}
