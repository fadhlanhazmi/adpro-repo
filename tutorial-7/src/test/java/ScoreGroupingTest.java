import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScoreGroupingTest {
    private Map<Integer, List<String>> groupedMap;
    private List<String> group11;
    private List<String> group12;
    private List<String> group15;

    @Before
    public void setUp() {
        Map<String, Integer> scores = new HashMap<String, Integer>();
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
        group11 = Arrays.asList("Charlie", "Foxtrot");
        group12 = Arrays.asList("Alice");
        group15 = Arrays.asList("Emi", "Bob", "Delta");
        groupedMap = ScoreGrouping.groupByScores(scores);
    }

    @Test
    public void testByScores11() {
        List<String> group11ByGroupedMap = groupedMap.get(11);
        assertTrue(group11ByGroupedMap.equals(group11));
    }

    @Test
    public void testByScores12() {
        List<String> group12ByGroupedMap = groupedMap.get(12);
        assertTrue(group12ByGroupedMap.equals(group12));
    }

    @Test
    public void testByScores15() {
        List<String> group15ByGroupedMap = groupedMap.get(15);
        assertTrue(group15ByGroupedMap.equals(group15));
    }
}