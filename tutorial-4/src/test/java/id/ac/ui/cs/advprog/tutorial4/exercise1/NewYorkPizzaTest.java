package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaTest {
    private PizzaStore nyStore;

    @Before
    public void setUp(){
        nyStore = new NewYorkPizzaStore();
    }

    @Test
    public void testVeggiePizza(){
        Pizza veggiePizza = nyStore.orderPizza("veggie");
        assertEquals("New York Style Veggie Pizza",veggiePizza.getName());
    }

    @Test
    public void testClamPizza(){
        Pizza clamPizza = nyStore.orderPizza("clam");
        assertEquals("New York Style Clam Pizza",clamPizza.getName());
    }

    @Test
    public void testCheesePizza(){
        Pizza cheesePizza = nyStore.orderPizza("cheese");
        assertEquals("New York Style Cheese Pizza", cheesePizza.getName());
    }
}
