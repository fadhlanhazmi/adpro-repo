package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DepokPizzaTest {
    private PizzaStore depokStore;

    @Before
    public void setUp(){
        depokStore = new DepokPizzaStore();
    }

    @Test
    public void testVeggiePizza(){
        Pizza veggiePizza = depokStore.orderPizza("veggie");
        assertEquals("Depok Style Veggie Pizza",veggiePizza.getName());
    }

    @Test
    public void testClamPizza(){
        Pizza clamPizza = depokStore.orderPizza("clam");
        assertEquals("Depok Style Clam Pizza",clamPizza.getName());
    }

    @Test
    public void testCheesePizza(){
        Pizza cheesePizza = depokStore.orderPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", cheesePizza.getName());
    }
}
