package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    private static Singleton uniqueInstances;

    private Singleton() {
        // Default constructor
    }

    public static Singleton getInstance() {
        if (uniqueInstances == null) {
            uniqueInstances = new Singleton();
        }
        return uniqueInstances;
    }
}
