package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class CookedClams implements Clams {
    public String toString() {
        return "The clams that have been cooked";
    }
}
