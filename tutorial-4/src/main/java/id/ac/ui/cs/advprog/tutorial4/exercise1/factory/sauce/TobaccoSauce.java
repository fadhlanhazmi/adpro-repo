package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class TobaccoSauce implements Sauce {
    public String toString() {
        return "Kinda spicy-sour sauce";
    }
}
