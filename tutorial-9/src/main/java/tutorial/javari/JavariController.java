package tutorial.javari;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.springframework.web.bind.annotation.RestController;
import tutorial.javari.animal.Animal;
import java.util.List;

@RestController
public class JavariController {

    @RequestMapping(value = "/javari",method = GET)
    public List<Animal> getListAnimal(){
        DataHandler.addAnimalsFromCSV();
        List<Animal> animals = DataHandler.getAnimals();
        if (animals.size() > 0){
            return animals;
        }
        else throw new IllegalArgumentException("No data in csv");
    }

    @RequestMapping(value = "/javari/{id}",method = GET)
    public Animal getAnimal(@PathVariable Integer id){
        Animal animal = DataHandler.findAnimal(id);
        if (animal != null){
            return animal;
        }
        else throw new IllegalArgumentException("No animal with that id");
    }

    @RequestMapping(value = "/javari{id}",method = DELETE)
    public Animal deleteAnimal(@PathVariable Integer id){
        Animal animal = DataHandler.deleteAnimal(id);
        if (animal != null){
            return animal;
        }
        else throw new IllegalArgumentException("No animal with that id, nothing deleted");
    }

    @RequestMapping(value = "/javari",method = POST)
    public Animal addAnimal(@PathVariable String json){
        return DataHandler.addAnimal(json);
    }
}
