package tutorial.javari;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataHandler {
    private static List<Animal> animals = null;

    public static List<Animal> getAnimals() {
        return animals;
    }

    public static void addAnimalsFromCSV(){
        if (animals != null) return;
        try {
            animals = new ArrayList<>();
            BufferedReader reader = new BufferedReader(new FileReader("animals.csv"));
            String line = reader.readLine();
            while (line != null){
                String[] properties = line.split(",");
                System.out.printf("0,%s 1,%s 2,%s 3,%s 4,%s 5,%s 6,%s \n",properties[0],properties[1],properties[2],properties[3],properties[4],properties[5],properties[6]);
                Animal animal = new Animal(Integer.parseInt(properties[0]),properties[1],properties[2],
                        Gender.parseGender(properties[3]),Double.parseDouble(properties[4]),
                        Double.parseDouble(properties[5]), Condition.parseCondition(properties[6]));
                animals.add(animal);
                line = reader.readLine();
            }
        }
        catch (IOException e){
            System.out.println("file not found");
        }
    }

    public static Animal findAnimal(int id){
        for (int i = 0; i < animals.size(); i++){
            if (animals.get(i).getId() == id){
                return animals.get(i);
            }
        }
        return null;
    }

    public static Animal deleteAnimal(int id){
        for (int i = 0; i < animals.size(); i++){
            if (animals.get(i).getId() == id){
                Animal animal = animals.get(i);
                animals.remove(animal);
                return animal;
            }
        }
        return null;
    }

    public static Animal addAnimal(String jsonInput){
        JSONObject json = new JSONObject(jsonInput);
        Animal animal = new Animal(json.getInt("id"), json.getString("type"),
                json.getString("name"), Gender.parseGender(json.getString("gender")),
                json.getDouble("length"), json.getDouble("weight"),
                Condition.parseCondition(json.getString("condition")));
        animals.add(animal);
        return animal;

    }
}
